import { check, fail } from 'k6';

function isEmpty(str) {
  return !str || str.length === 0;
}

function performCheck(response, condition, message, printOnError, failOnError) {
  const result = check(response, { [message]: condition });

  if (!result) {
    if (printOnError && response.body) {
      console.log('Unexpected response: ' + response.body);
    }
    if (failOnError) {
      fail(message);
    }
  }

  return result;
}

export function checkStatusHelper({
  response,
  expectedStatus,
  expectedContent,
  failOnError,
  printOnError,
  dynamicIds,
}) {
  if (isEmpty(expectedStatus) && isEmpty(expectedContent)) {
    console.warn(
      `No expected status or content specified in call to checkStatus for URL ${response.url}`,
    );
    return;
  }

  let url = response.url;

  if (expectedStatus) {
    performCheck(
      response,
      (r) => r.status === expectedStatus,
      `${response.request.method} ${url} status ${expectedStatus}`,
      printOnError,
      failOnError,
    );
  }

  if (expectedContent) {
    performCheck(
      response,
      (r) => r.body.includes(expectedContent),
      `"${expectedContent}" in ${url} response`,
      printOnError,
      failOnError,
    );
  }

  if (dynamicIds) {
    dynamicIds.forEach((dynamicId) => {
      if (response.url.includes(dynamicId)) {
        url = url.replace(dynamicId, '[id]');
      }
    });
  }
}
