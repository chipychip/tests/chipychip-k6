import { SharedArray } from 'k6/data';
import { Counter } from 'k6/metrics';

export const users = new SharedArray('users.json', function () {
  return JSON.parse(open('../data/users.json')).users;
});

export const errorCounter = new Counter('errors');
