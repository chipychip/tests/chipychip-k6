const envConfig = {
  dev: {
    BASE_URL: 'http://localhost:3000',
  },
  prod: {
    BASE_URL: 'https://chipchipy.netlify.app/',
  },
};

const config = envConfig[__ENV.ENVIRONMENT] || envConfig['prod'];

export const BASE_URL = config.BASE_URL;

export const FIREBASE = {
  AUTH: 'https://identitytoolkit.googleapis.com',
  STORAGE: 'https://firebasestorage.googleapis.com',
  DB: 'https://firestore.googleapis.com',
};
