export const workloadConfig = {
  smoke: [{ duration: '30s', target: 5 }],
  average: [
    { duration: '1m', target: 100 },
    { duration: '4m', target: 100 },
    { duration: '1m', target: 0 },
  ],
  stress: [
    { duration: '1m', target: 700 },
    { duration: '4m', target: 700 },
    { duration: '1m', target: 0 },
  ],
  soak: [
    { duration: '5m', target: 100 },
    { duration: '5m', target: 100 },
    { duration: '5m', target: 0 },
  ],
  spike: [
    { duration: '1m', target: 500 },
    { duration: '1m', target: 0 },
  ],
  breakpoint: [
    { duration: '1m', target: 100 },
    { duration: '1m', target: 200 },
    { duration: '1m', target: 300 },
  ],
};
