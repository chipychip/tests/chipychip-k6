
FROM loadimpact/k6:latest

WORKDIR /app

COPY . .

ENTRYPOINT ["k6", "cloud", "/app/tests/main.js"]