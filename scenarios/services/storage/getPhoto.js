import http from 'k6/http';
import { group } from 'k6';
import { FIREBASE } from '../../../config/settings.js';
import { checkStatusHelper } from '../../../helpers/checkStatusHelper.js';
import { errorCounter } from '../../../helpers/common.js';

export default function (data) {
  const pathToPhotos = '%2Fimages%2Fphoto%2F&delimiter=%2F';
  const url = `${FIREBASE.STORAGE}/v0/b/${__ENV.REACT_APP_STORAGE_BUCKET}/o?prefix=${data.localId}${pathToPhotos}`;

  const params = {
    headers: {
      Authorization: `Firebase ${data.idToken}`,
    },
  };
  group('get all photo', function () {
    let res = http.get(url, params);

    checkStatusHelper({
      response: res,
      expectedStatus: 200,
      expectedContent: 'items',
      failOnError: true,
      printOnError: true,
    });

    if (!res.json().items) {
      errorCounter.add(1);
    }
  });
}

export function canNotGetPhotoWithIncorrectToken() {
  const pathToPhotos = '%2Fimages%2Fphoto%2F&delimiter=%2F';
  const url = `${FIREBASE.STORAGE}/v0/b/${__ENV.REACT_APP_STORAGE_BUCKET}/o/${data.localId}${pathToPhotos}`;
  const params = {
    headers: {
      Authorization: `Bearer incorrect_token`,
    },
  };
  const res = http.get(url, params);
  checkStatusHelper({
    response: res,
    expectedStatus: 403,
    expectedContent: 'error',
    failOnError: true,
    printOnError: true,
  });

  if (res.json().items) {
    errorCounter.add(1);
  }
}
