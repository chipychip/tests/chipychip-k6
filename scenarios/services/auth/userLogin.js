import http from 'k6/http';
import { checkStatusHelper } from '../../../helpers/checkStatusHelper.js';
import { FIREBASE } from '../../../config/settings.js';
import { errorCounter, users } from '../../../helpers/common.js';
import { group } from 'k6';

export default function () {
  const url = `${FIREBASE.AUTH}/v1/accounts:signInWithPassword?key=${__ENV.REACT_APP_API_KEY}`;
  const user = users[Math.floor(Math.random() * users.length)];
  const payload = JSON.stringify(user);
  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  group(`login user: ${user.email}`, function () {
    let res = http.post(url, payload, params);
    checkStatusHelper({
      response: res,
      expectedStatus: 200,
      expectedContent: 'idToken',
      failOnError: true,
      printOnError: true,
    });

    if (!res.json().idToken) {
      errorCounter.add(1);
    }
  });
  const jar = http.cookieJar();
  jar.set(url, 'authToken', res.json().idToken);
  return res.json();
}
