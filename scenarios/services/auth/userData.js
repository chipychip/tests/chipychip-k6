import { group } from 'k6';
import http from 'k6/http';
import { checkStatusHelper } from '../../../helpers/checkStatusHelper.js';
import { FIREBASE } from '../../../config/settings.js';
import { errorCounter } from '../../../helpers/common.js';

export default function (data) {
  const payload = JSON.stringify({ idToken: data.idToken });
  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  group('user data', function () {
    const urlUserData = `${FIREBASE.AUTH}/v1/accounts:lookup?key=${__ENV.REACT_APP_API_KEY}`;
    let res = http.post(urlUserData, payload, params);

    checkStatusHelper({
      response: res,
      expectedStatus: 200,
      expectedContent: 'users',
      failOnError: true,
      printOnError: true,
    });

    if (!res.json().users) {
      errorCounter.add(1);
    }
  });
}
