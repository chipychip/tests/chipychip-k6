import http from 'k6/http';
import { sleep } from 'k6';
import { BASE_URL } from '../config/settings.js';
import { thresholdsConfig } from '../config/thresholds.js';
import * as tracing from 'k6/experimental/tracing';
import { workloadConfig } from '../config/workloads.js';
import getPhoto, {
  canNotGetPhotoWithIncorrectToken,
} from '../scenarios/services/storage/getPhoto.js';
import userLogin from '../scenarios/services/auth/userLogin.js';
import userData from '../scenarios/services/auth/userData.js';

tracing.instrumentHTTP({
  propagator: 'w3c',
});

export const options = {
  scenarios: {
    positiveCase: {
      executor: 'ramping-vus',
      startVUs: 0,
      stages: workloadConfig.smoke,
      gracefulRampDown: '0s',
      exec: 'userLoginAndFetchData',
    },
  },
  thresholds: thresholdsConfig.common,
  ext: {
    loadimpact: {
      distribution: {
        'amazon:de:frankfurt': {
          loadZone: 'amazon:de:frankfurt',
          percent: 100,
        },
      },
    },
  },
};

export function userLoginAndFetchData() {
  const data = userLogin();
  userData(data);
  getPhoto(data);

  sleep(1);
}

export function negativeCase() {
  const data = userLogin();
  userData(data);
  canNotGetPhotoWithIncorrectToken();

  sleep(1);
}

export function teardown() {
  const jar = http.cookieJar();
  jar.set(BASE_URL, 'authToken', '');
}
